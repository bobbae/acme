package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"sync"

	log "gitlab.com/bobbae/logrus"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"golang.org/x/crypto/acme/autocert"
)

type commonBalancer struct {
	targets []*middleware.ProxyTarget
	mutex   sync.RWMutex
}

type singleBalancer struct {
	*commonBalancer
}

func (b *commonBalancer) AddTarget(target *middleware.ProxyTarget) bool {
	for _, t := range b.targets {
		if t.Name == target.Name {
			return false
		}
	}
	b.mutex.Lock()
	defer b.mutex.Unlock()
	b.targets = append(b.targets, target)
	return true
}

// RemoveTarget removes an upstream target from the list.
func (b *commonBalancer) RemoveTarget(name string) bool {
	b.mutex.Lock()
	defer b.mutex.Unlock()
	for i, t := range b.targets {
		if t.Name == name {
			b.targets = append(b.targets[:i], b.targets[i+1:]...)
			return true
		}
	}
	return false
}

func (b *singleBalancer) Next() *middleware.ProxyTarget {
	b.mutex.RLock()
	defer b.mutex.RUnlock()
	log.Debugf("return %v", b.targets[0])
	return b.targets[0]
}

//NewSingleBalancer returns the single target
func NewSingleBalancer(targets []*middleware.ProxyTarget) middleware.ProxyBalancer {
	b := &singleBalancer{commonBalancer: new(commonBalancer)}
	b.targets = targets
	return b
}

func main() {
	fmt.Println(os.Args)
	domainName := flag.String("domain", "", "domain name")
	cacheDir := flag.String("cacheDir", "/var/www/.cache", "domain name")
	group := flag.String("group", "", "group path,url")
	acme := flag.Bool("acme", false, "auto certificate generation via ACME protocol")
	debug := flag.Bool("debug", false, "debug")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	e := echo.New()
	e.HideBanner = true

	if *domainName == "" {
		log.Fatalf("domain name required")
	}
	log.Debugf("domain name %s", *domainName)

	e.Use(middleware.Recover())
	e.Use(middleware.Logger())

	if *group != "" {
		url1, err := url.Parse("http://10.101.101.202:8081")
		if err != nil {
			e.Logger.Fatal(err)
		}
		targets := []*middleware.ProxyTarget{
			{
				URL: url1,
			},
		}
		g := e.Group("/test-1")
		g.Use(middleware.Proxy(NewSingleBalancer(targets)))
	}

	if *acme {
		e.AutoTLSManager.HostPolicy = autocert.HostWhitelist(*domainName)
		e.AutoTLSManager.Cache = autocert.DirCache(*cacheDir)

		e.GET("/", func(c echo.Context) error {
			return c.HTML(http.StatusOK, `
			<h1>Welcome to Echo!</h1>
			<h3>TLS certificates automatically installed from Let's Encrypt :)</h3>
		`)
		})

		e.Logger.Fatal(e.StartAutoTLS(":443"))
	} else {
		proxy := httputil.NewSingleHostReverseProxy(&url.URL{
			Scheme: "http",
			Host:   "10.101.101.202:8081",
		})
		e.Any("/*", echo.WrapHandler(proxy))

		e.Logger.Fatal(e.StartTLS(":443", *cacheDir+"/cert.pem", *cacheDir+"/key.pem"))
	}
}
