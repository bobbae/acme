default: app-build docker-build docker-push 

app-build:
	go get ./...
	CGO_ENABLED=0 go build

docker-build:
	docker build -t bobbae/acme .

docker-push:
	docker push bobbae/acme

docker-run:
	docker run --rm --name acme111 -d -v `pwd`/certs:/var/www/.cache -v /etc/ssl/certs:/etc/ssl/certs --network host bobbae/acme -domain medge.gq -debug

